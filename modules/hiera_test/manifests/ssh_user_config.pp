class hiera_test::ssh_user_config (
  $ssh_keys                 = false,
  $install_ssh_keys         = false,
  $local_config_path        = '/home/deploy/.ssh',
  $local_config_file        = 'config',
  $local_config_user        = 'deploy',
  $local_config_group       = false,
  $local_config_mode        = '0600',
  $concat_fragments         = '10',
  $default_ops_ssh          = true,
  $concat_head_fragment     = '/etc/ssh/ssh_header.erb',
  $concat_specific_fragment = '/etc/ssh/header_specific.erb',
) {

  if $local_config_group {
    $concat_group = $local_config_group
  } else {
    $concat_group = $local_config_user
  }

  concat { "${local_config_path}/${local_config_file}":
    owner => $local_config_user,
    group => $concat_group,
    mode  => $local_config_mode,
  }

  if $ssh_keys {
    concat::fragment { "${local_config_path}/${local_config_file}_specific":
      target  => "${local_config_path}/${local_config_file}",
      order   => '10',
      content => template("${module_name}/${concat_specific_fragment}"),
    }

    if is_hash($ssh_keys) {
      $ssh_private_keys = keys($ssh_keys)
      $all_keys = hiera_hash('hiera_test::ssh_keys', undef )
      notify { "All the private keys are [ $ssh_private_keys ]": }
      create_resources(hiera_test::modules::ssh_config, $all_keys)
    }
  }

}
