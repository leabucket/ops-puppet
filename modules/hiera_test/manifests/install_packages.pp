define hiera_test::install_packages ( $version = 'installed' ) {
 if is_hash($title) {
  $data_keys = keys($title)
  $package_name = $data_keys[0]

  if has_key($title, 'version') {
   $install_package = $package_name
   $install_version = $title[version]
  }
 } else {
  $install_package = $title
  $install_version = $version
 }
  package { $install_package:
   ensure => $version,
  }

 }
