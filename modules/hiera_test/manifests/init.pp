class hiera_test ($whoami = false,
                  $create_files = false, $install_packages = false, $install_ssh_keys = false, $ssh_keys = false, $firstname = undef)
{

  #create_resources (hiera_test::install_packages, $install_packages)
  include hiera_test::modules::puppet_agent_graphs
  #notify {"Package created":}
#  if $create_files {
#   if is_hash($create_files) {
#    $filename = keys($create_files)
#    notify { "sample for $filename": }
#    create_resources(hiera_test::modules::file, $create_files)
#   } else {
#    hiera_test::modules::file { $create_files: }
#    }
#  }

#  file_line { 'remove_java_opts':
#    ensure    => absent,
#    path      => '/tmp/jenkins',
#    match     => '#JENKINS.*',
#    match_for_absence => true,
#  }

  if $ssh_keys {
    class { 'hiera_test::ssh_user_config':
          ssh_keys => $ssh_keys,
    }
  }
# if $ssh_keys {
# class { 'hiera_test::ssh_user_config':
#          ssh_keys => $ssh_keys,
#        }
# }

## Tetst the PICK function in Puppet

#  $user_firstname = pick ("$firstname", 'Leadel')

#  notify {"User firstname is $user_firstname": }
}
