 class hiera_test::modules::puppet_agent_graphs {
 ini_setting { 'enable_graph':
  ensure => present,
  path   => '/etc/puppet/puppet.conf',
  section => 'agent',
  setting => 'graph',
  value   => 'true'
 }
 $safe_role = regsubst($::lhotse_role, '-', '_', 'G')
 notify {"safe role is now $safe_role":}
}
