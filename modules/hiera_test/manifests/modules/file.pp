define hiera_test::modules::file (
 $ensure   = 'present',
 $template = "${caller_module_name}/${title}.erb",
 $source   = false,
 $replace  = true,
 $mode     = false,
 $user     = false,
 $group    = "$user",
 $notifies   = false,
 $content    = false
) {
 $filename = $title
 notify { "Start creating the file -- $filename --": }
 if $user {
  $user_mode = $user
 }

 if $group {
  $group_mode = $group
 }

 if $mode {
  $file_mode = $mode
 }

 if $ensure == 'absent' {
  file { $title:
   ensure => 'absent',
  }
 } elsif $ensure == 'directory' {
     unless $user_mode {
      $user_mode = 'root'
     }

     unless $group_mode {
      $group_mode = 'root'
     }

     unless $file_mode {
      $file_mode = '0755'
     }

     file { $title:
      ensure  => $ensure,
      owner   => $user_mode,
      group   => $group_mode,
      mode    => $file_mode,
  #    content => $content,
     }
   } elsif $source {
      unless $user_mode {
       $user_mode = 'root'
      }

      unless $group_mode {
       $group_mode = 'root'
      }

      unless $file_mode {
       $file_mode = '0755'
      }

      if $notifies {
       file { $title:
        ensure  => $ensure,
        owner   => $user_mode,
        group   => $group_mode,
        mode    => $file_mode,
        notify  => $notifies,
        source  => $source,
        replace => $replace,
       }
      }
     } else {
    unless $user_mode {
      $user_mode = 'root'
    }
    unless $group_mode {
      $group_mode = 'root'
    }
    unless $file_mode {
      $file_mode = '0644'
    }
    if $notifies {
      file { $title:
        ensure  => $ensure,
        owner   => $user_mode,
        group   => $group_mode,
        mode    => $file_mode,
        content => template($template),
        notify  => $notifies,
        replace => $replace,
      }
    } else {
      file { $title:
        ensure  => $ensure,
        owner   => $user_mode,
        group   => $group_mode,
        mode    => $file_mode,
        content => template($template),
        replace => $replace,
      }
    }
  }

}
