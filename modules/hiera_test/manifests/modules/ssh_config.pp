define hiera_test::modules::ssh_config (
  $system_user           = 'deploy',
  $forwardagent          = false,
  $identitiesonly        = false,
  $ssh_keys              = false,
  $logl                  = false,
  $checkhostip           = false,
  $userknownhostsfile    = false,
  $config_target         = false,
  $disable_key_rollout   = false,
  $concat_order          = '10',
  $stricthostkeychecking = false,
  $ssh_config_target     = '/etc/ssh/ssh_config.erb'
) {


  if ('@' in $title) {
    $arr = split($title, '@')
    $user = $arr[0]
    $host = $arr[1]
 }else {
   $host        = $title
 }
  notify {" User = ${user} and host = ${host}": }

  if $config_target {
    $concat_target = $config_target
  } else {
    $concat_target = "/home/${system_user}/.ssh/config"
  }

  concat::fragment { "${concat_target}_${title}":
    target  => $concat_target,
    order   => $concat_order,
    content => template("${module_name}/${ssh_config_target}")
  }

}
