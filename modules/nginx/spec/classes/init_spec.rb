require 'spec_helper'
describe 'nginx' do
  let(:title) { 'nginx' }
  let(:node) { 'test.example.com' }
  let(:facts) { {
    :fact1 => 'value 1',
    :fact2 => 'value 2',

    } }
  let(:params) { {} }

  it { is_expected.to compile }
  it { is_expected.to compile.with_all_deps }
  it { is_expected.to contain_package('nginx').with(ensure: 'present') }
  it { is_expected.to contain_file('/var/www/index.html')
    .with(
      :ensure => 'file',
      :require => 'Package[nginx]',
    )
  }
  #
  # context 'with defaults for all parameters' do
  #   it { should contain_class('nginx') }
  # end
end
