class user_account::user_account_profile::ensure_file ($filename=false, $fileContent=false) {
  $current_date = inline_template('<%= Time.new.inspect %>')
  $person_name = hiera('user_account::username')
  $greetings = "Good Morning $person_name. Hope you are doing fine today ${current_date}"
 # $greetings = "Good Morning %{hiera('user_account::username')}. Hope you are doing fine today ${current_date}"
  # file { $filename:
  #   ensure  => absent,
  #   content => "$greetings"
  # }
  if $filename {
    if is_hash($filename) {
      create_resources(user_account::user_account_profile::user_files, $filename)
    }else {
    user_account::user_account_profile::user_files( $filename )
   }
  }
}
