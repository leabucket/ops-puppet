define user_account::user_account_profile::user_files (
  $ensure = 'present',
  $content = false,
  $owner_file   = false,
  $mode_file    = false,
  ) {

  if $content {
    $file_content = $content
    file { $title:
      ensure => $ensure,
      content => $file_content,
    }
  } else {
    notify { "owner = $owner and mode =$mode ": }
    file { $title:
     ensure => $ensure,
     owner  => $owner_file,
     mode  => $mode_file
    }
  }
}
