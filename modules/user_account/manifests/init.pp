class user_account ($username = 'jamie', $user_mode = '0700') {

 user {$username:
    ensure => present,
  }
 #notify {"USER = $username was created":}

 # notify {$greetings:}
 #include user_account
 #include user_account::user_account_profile::ensure_file
 #include user_account::user_account_profile::ensure_service
notify { "$username => $user_mode":}
 $tmp_file = { '/home/deploy/EXAMPLE' => { ensure => directory, owner_file => $username, mode_file => $user_mode}}

 create_resources( user_account::user_account_profile::user_files, $tmp_file)

}
