# test on how to use create resource with defaults

$defaults = {
   comment => 'some user created',
   ensure   => 'present',
 }

$user_data = {
  'leadel2' => {
    ensure => 'absent',
    comment => 'my name is Leadel',
  },
  'nicole2' => {
    ensure => 'some',
    comment => 'my name is Nicole',
  },
  'jamie2'  => {
    ensure => 'bad',
    comment => 'my name is Jamie'
  }
}

define display_user ( $comment = true , $ensure = true ) {

  $dname_var = $title
  notify {"User Title: ${dname_var}": }
  notify {"User Comment:  ${comment}": }
  notify {"User ensure: ${ensure}": }
}
create_resources(display_user, $user_data, $defaults)
