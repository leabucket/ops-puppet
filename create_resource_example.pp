define create_resource_example::recursion::numbers ( $count ) {
 $resource_name = $title
 notify { "count is now ${count} and resource name is ${resource_name}": }
 $reduce_count = inline_template('<%= @count.to_i - 1 %>')
 if "${reduce_count}" == '0' {
   notify { 'Done Counting':}
 }else {
  ## Do the recurse stuffs
  create_resource_example::recursion::numbers { "count-${reduce_count}":
   count => $reduce_count,
 #  resource_name => $title
   }
 }
}

## start the recursion process

#recursion { 'start':
# count => 5
# }

class create_resource_example ( $modName = false, $count_hash = { 'count-10' => { count => '10', } } ) {
 if $modName {
  notify {"Module name is set to ${modName}": }
 } else {
   notify {'NO nodName value set':}
 }
 create_resources (create_resource_example::recursion::numbers, $count_hash)
 }

 # you can include the class directly
 # include recursion::pprofile

 ## or you can call the class as a Parameterized class
 class { 'create_resource_example':
  #  modName => "Fuck"
  }
